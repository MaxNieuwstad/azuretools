# Azure KeyVault Tools

This set of scripts transforms a json file to a set of commands to populate a keyvault *and* add the settings to an Azure AppService. 
The KeyVault will have to be created manually. 
The app service has to be granted access to the vault as well. 


# Usage

```sh
optional arguments:
  -h, --help            show this help message and exit
  --keyvault_name KEYVAULT_NAME
                        Name of the keyvault
  --resource_group RESOURCE_GROUP
                        ResourceGroup the keyvault and appservice are in
  --app_service_name APP_SERVICE_NAME
                        Name of the appservice whose settings are to be set
  --path_to_appsettings PATH_TO_APPSETTINGS
                        Path to the appsettings file to parse
  --keyvault_purge KEYVAULT_PURGE
                        Generate keyvault delete commands
  --keyvault_delete KEYVAULT_DELETE
                        Generate keyvault pruge commands
  --appsetting_remove APPSETTING_REMOVE
                        Generate appsettings remove commands

```


# Example

```sh
$> python main.py --keyvault_name kv-test --resource_group test-resource-group --app_service_name backoffice  --path_to_appsettings "/home/lucas/projecten/hiltermann/hiltermann-core/src/Hiltermann.Backoffice/appsettings.Production.json" --keyvault_delete true

### Keyvault ###
az keyvault secret set --vault-name 'kv-test' --name 'ApiBaseAddress' --value 'https://api-test.live'
az keyvault secret set --vault-name 'kv-test' --name 'Auth0-Backoffice-Domain' --value 'test-project.eu.auth0.com'


az keyvault secret delete --vault-name 'kv-test' --name 'ApiBaseAddress'
az keyvault secret delete --vault-name 'kv-test' --name 'Auth0-Backoffice-Domain'

### AppSettings ###
az webapp config appsettings set --name 'backoffice' --resource-group 'test-resource-group' --settings 'ApiBaseAddress=@Microsoft.KeyVault(VaultName=kv-test;SecretName=ApiBaseAddress)' \
'Auth0__Backoffice__Domain=@Microsoft.KeyVault(VaultName=kv-test;SecretName=Auth0-Backoffice-Domain)' 
```
