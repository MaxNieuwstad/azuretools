import unittest


def handle_list(lst: [], keys: [str]):
    for i, item in enumerate(lst):
        if isinstance(item, dict):
            yield from flatten_json(item, keys + [str(i)])
        elif isinstance(item, list):
            yield from handle_list(item, keys + [str(i)])
        else:
            yield keys + [str(i)], item


def flatten_json(json_dict: dict, keys=None):
    if keys is None:
        keys = []

    for k, v in json_dict.items():
        if isinstance(v, dict):
            yield from flatten_json(v, keys + [k])
        elif isinstance(v, list):
            yield from handle_list(v, keys + [k])
        else:
            yield keys + [k], v


class Tests(unittest.TestCase):

    def test_single(self):
        d = {'test': 12}
        result = flatten_json(d)
        strings = ['-'.join(s[0]) + ':' + f'{s[1]}' for s in result]
        self.assertEqual('test:12', strings[0])

    def test_double(self):
        d = {'test': 12, 'test2': 12}
        result = flatten_json(d)
        strings = ['-'.join(s[0]) + ':' + f'{s[1]}' for s in result]
        self.assertEqual('test:12', strings[0])
        self.assertEqual('test2:12', strings[1])

    def test_flatter(self):
        d = {'test': 12,
             'Test': {'Value': 12},
             'Test3': {
                 'Value': 13,
                 'Value2': 2,
             },
             'Test4': {'Value': {"abc": 13}}
             }
        result = flatten_json(d)
        strings = ['-'.join(s[0]) + ':' + f'{s[1]}' for s in result]
        self.assertEqual('test:12', strings[0])
        self.assertEqual('Test-Value:12', strings[1])
        self.assertEqual('Test3-Value:13', strings[2])
        self.assertEqual('Test3-Value2:2', strings[3])
        self.assertEqual('Test4-Value-abc:13', strings[4])

    def test_array(self):
        d = {'test': 12,
             'Test': [1, 2, 3],
             'test3': [{'a': 1}, {'ab': 12}]}
        result = list(flatten_json(d))
        strings = ['-'.join(s[0]) + ':' + f'{s[1]}' for s in result]
        self.assertEqual('test:12', strings[0])
        self.assertEqual('Test-0:1', strings[1])
        self.assertEqual('Test-1:2', strings[2])
        self.assertEqual('Test-2:3', strings[3])
        self.assertEqual('test3-0-a:1', strings[4])
        self.assertEqual('test3-1-ab:12', strings[5])
