keyvault_level_sep = "_"
docker_level_sep = "__"


def create_terraform_vars_file(rows: [tuple], key_start: str):
    def create_row(key: [str], key_value: str, base: str):
        key_name = base + keyvault_level_sep.join(key)

        if isinstance(key_value, bool):
            return f'{key_name} = "{key_value}"'

        if isinstance(key_value, int):
            return f'{key_name} = {key_value}'

        key_value = key_value.replace('\n', '')
        return f'{key_name} = "{key_value}"'

    return [create_row(r[0], r[1], key_start) for r in rows]


def create_terraform_variable_list(rows: [tuple], key_start: str):
    def create_row(key: [str], key_value: str, base: str):
        key_name = base + keyvault_level_sep.join(key)
        if isinstance(key_value, bool):
            key_type = 'bool'
        elif isinstance(key_value, int):
            key_type = 'number'
        else:
            key_type = 'string'

        return f'variable "{key_name}" ' + '{\n  type = ' + key_type + '\n}'

    return [create_row(r[0], r[1], key_start) for r in rows]


def create_terraform_app_settings_list(rows: [tuple], key_right: str):
    def create_row(key: [str], right: str):
        right_key_name = right + keyvault_level_sep.join(key)
        left_key_name = docker_level_sep.join(key)
        return f'{left_key_name} = var.{right_key_name}'

    return [create_row(r[0], key_right) for r in rows]
